# import libraries
import urllib.request
from bs4 import BeautifulSoup
import EnterGsheet
from datetime import datetime
import time


# translate to columns
def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string


def startupdate(debugsheet):
    tcell_list = debugsheet.range('A1:A15')
    tcell_list[2].value = int(tcell_list[2].value) + 1
    # cell_list[7].value = 'LAST UPDATED START'
    tcell_list[8].value = str(datetime.now())
    debugsheet.update_cells(tcell_list, 'USER_ENTERED')


def finishupdate(debugsheet):
    tcell_list = debugsheet.range('A1:A15')
    tcell_list[4].value = int(tcell_list[4].value) + 1
    # cell_list[9].value = 'LAST UPDATED END'
    tcell_list[10].value = str(datetime.now())
    debugsheet.update_cells(tcell_list, 'USER_ENTERED')

def animeremoved(debugsheet):
    tcell_list = debugsheet.range('A1:A15')
    tcell_list[14].value = int(tcell_list[14].value) + 1
    debugsheet.update_cells(tcell_list, 'USER_ENTERED')

def animeadded(debugsheet):
    tcell_list = debugsheet.range('A1:A15')
    tcell_list[12].value = int(tcell_list[12].value) + 1
    debugsheet.update_cells(tcell_list, 'USER_ENTERED')


def stripstring(thestring):
    thestring = thestring.strip()
    thestring = thestring.replace('\n', '')
    return thestring


# see stackoverflow 6618515/sorting-list-based-on-values-from-another-list
def sortbyanimename(namearray, rating):
    return [namearray for (namearray, rating) in sorted(zip(namearray, rating), key=lambda pair: pair[0])],\
           [rating for (namearray, rating) in sorted(zip(namearray, rating), key=lambda pair: pair[0])]


def browsetowebsite(website_url):
    page = urllib.request.urlopen(website_url)
    soup_ = BeautifulSoup(page, 'html.parser')
    return soup_


def updatecells(sheet_,debugsheet_,score_array_):
    current_column_ = colnum_string(int(debugsheet_.cell(5, 1).value) + 2)  # +2 for name column
    # get the range of array to be changed
    cell_list_ = sheet_.range(current_column_ + '1:' + current_column_ + str(len(score_array_) + 1))  # +1 for the date row
    # assign date to first row
    cell_list_[0].value = str(datetime.now().date())
    # assign scorelist_ as the sorted list by  name
    scorelist_ = animetuple[1]
    for i_ in range(1, len(cell_list_)):
        j_ = i_ - 1
        cell_list_[i_].value = scorelist_[j_]
    sheet_.update_cells(cell_list_, 'USER_ENTERED')
    finishupdate(debugsheet_)
    
    
def getanimetuple(soup_):
    score_array = soup_.find_all(class_="score")
    score_array = [stripstring(score.text) for score in score_array]
    title_array = soup_.find_all(class_="title-text")
    title_array = [stripstring(title.text) for title in title_array]
    airdate_array = soup_.find_all(class_="remain-time")
    number_ep_array = soup_.find_all(class_="eps")
    season_title_ = soup_.find_all(class_="on")
    season_title_ = stripstring(season_title_[0].text)

    animetuple_ = sortbyanimename(title_array, score_array)
    return [animetuple_, season_title_]

link='https://myanimelist.net/anime/season'
soup = browsetowebsite(link)
time.sleep(2)


seasons_array_parent = list()
#gets the values for horizontal bar
for ultag in soup.find_all('div', {'class': 'horiznav_nav'}):
    seasons_array_parent.append(ultag.find_all('a'))
seasons_array_parent=seasons_array_parent[0]
seasons_array = [stripstring(season.text) for season in seasons_array_parent]
i=0
# checks for index for current season
for season_link in seasons_array_parent:
    if season_link.get('href')==link:
        break
    i += 1

#get current season and one before it
seasons_array = seasons_array[i-1:i+1]
seasons_array_parent = seasons_array_parent[i-1:i+1]
print(seasons_array)
#seasons_array_zipped = zip(seasons_array[i-1:i+1], seasons_array_parent[i-1:i+1])

gsheet = EnterGsheet.authsheet()
worksheetID_list = gsheet.worksheets()
worksheet_list = list()

for i in range(0, len(worksheetID_list)):
    worksheet_list.append(worksheetID_list[i].title)


for index, season_name in enumerate(seasons_array):
    print(season_name)
    # if sheet exists but is not current sheet
    link = seasons_array_parent[index].get('href')
    soup = browsetowebsite(link)

    temp_results = getanimetuple(soup)
    season_title = temp_results[1]
    animetuple = temp_results[0]
    title_array = animetuple[0]
    score_array = animetuple[1]
    time.sleep(5)

    if season_name in worksheet_list:
        #rebrowses to new website


        sheet = gsheet.worksheet(season_title)
        debugsheet = gsheet.worksheet(season_title + ' Debug Information')

        startupdate(debugsheet)

        temp_col = sheet.col_values(1)
        temp_col.pop(0)
        animes_on_sheet = temp_col  # gets all anime names in sheet

        # use symmetric difference and also find out the indices of differences
        animes_on_sheet_set = set(animes_on_sheet)
        title_array_set = set(title_array)
        animes_removed = animes_on_sheet_set.difference(title_array_set)
        animes_added = title_array_set.difference(animes_on_sheet_set)
        if len(animes_removed) > 0:
            # animes_on_sheet = sorted(animes_on_sheet)
            # index_removed = []
            current_column = colnum_string(int(debugsheet.cell(3, 1).value) + 1)  # +1 for name column
            current_column_plusone = colnum_string(int(debugsheet.cell(3, 1).value) + 1+1)
            #index_removed.append(animes_on_sheet.index(anime))
            for anime in animes_removed:
                current_removed = int(debugsheet.cell(15, 1).value) + 1 #+1 for starting at 1 for index
                # +2 for popping the date row out and for index starting at 1 on sheet
                current_anime_index= animes_on_sheet.index(anime)+2
                cell_list = sheet.range('A' + str(current_anime_index) + ':' + current_column + str(current_anime_index))
                debug_cell_list = debugsheet.range('B' + str(current_removed) +':'+ current_column_plusone + str(current_removed))
                for i in range(0, len(cell_list)):
                    debug_cell_list[i].value=cell_list[i].value
                debugsheet.update_cells(debug_cell_list,'USER_ENTERED')
                animeremoved(debugsheet)
        # removes them from spreadsheet
        while len(animes_removed) > 0:
            temp_col = sheet.col_values(1)
            temp_col.pop(0)
            animes_on_sheet = temp_col  # gets all anime names in sheet
            temp = animes_removed.pop()
            sheet.delete_row(animes_on_sheet.index(temp)+2)
            time.sleep(20)
        while len(animes_added) > 0:
            temp_col = sheet.col_values(1)
            temp_col.pop(0)
            animes_on_sheet = temp_col  # gets all anime names in sheet
            temp = animes_added.pop()
            animes_on_sheet.append(temp)
            index_helper=sorted(animes_on_sheet)
            sheet.insert_row([temp], index_helper.index(temp) + 2, 'USER_ENTERED')
            animeadded(debugsheet)
            time.sleep(20)
        updatecells(sheet, debugsheet, score_array)
    else:


        gsheet.add_worksheet(season_title, 500, 256)
        gsheet.add_worksheet(season_title + ' Debug Information', 500, 256)
        sheet = gsheet.worksheet(season_title)
        debugsheet = gsheet.worksheet(season_title + ' Debug Information')
        # populate the debug sheet
        cell_list = debugsheet.range('A1:A15')
        cell_list[0].value = season_title
        cell_list[1].value = 'NUMBER OF DAYS UPDATED (START)'
        cell_list[2].value = 0
        cell_list[3].value = 'NUMBER OF DAYS UPDATED (FINISH)'
        cell_list[4].value = 0
        cell_list[5].value = 'DATE BEGAN'
        cell_list[6].value = str(datetime.now().date())
        cell_list[7].value = 'LAST UPDATED START'
        cell_list[8].value = str(datetime.now())
        cell_list[9].value = 'LAST UPDATED END'
        cell_list[10].value = 'NOTHING'
        cell_list[11].value = 'ANIMES ADDED'
        cell_list[12].value = '0'
        cell_list[13].value = 'ANIMES REMOVED'
        cell_list[14].value = '0'
        debugsheet.update_cells(cell_list, 'USER_ENTERED')  # update debugsheet
        cell_list = sheet.range('A1:A' + str(len(title_array) + 1))  # +1 for the date row
        startupdate(debugsheet)
        # fill the animetuple using sortbyanimename
        # name_need_sorting = []
        # for i in range(1, len(cell_list)):  # start at 1 to skip date row
        #     j = i - 1#
        #     name_need_sorting.append(title_array[j])
        # score_need_sorting = []
        # for i in range(1, len(cell_list)): # start at 1 to skip date row
        #     j = i - 1
        #     score_need_sorting.append(score_array[j])
        # animetuple= sortbyanimename(name_need_sorting,score_need_sorting)
        #assign namelist as the alphabetically sorted list
        namelist= animetuple[0]
        for i in range(1, len(cell_list)):
            j = i - 1
            cell_list[i].value = namelist[j]
        sheet.update_cells(cell_list, 'USER_ENTERED')
        updatecells(sheet, debugsheet, score_array)
