import urllib.request
from bs4 import BeautifulSoup
import EnterGsheet
from datetime import datetime

#translate to columns
def colnum_string(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string

def startupdate(sheet):
    tcell_list= sheet.range('A1:A11')
    tcell_list[2].value = int(tcell_list[2].value)+1
    #cell_list[7].value = 'LAST UPDATED START'
    tcell_list[8].value = str(datetime.now())
    sheet.update_cells(tcell_list,'USER_ENTERED')


def finishupdate(sheet):
    tcell_list = sheet.range('A1:A11')
    tcell_list[4].value = int(tcell_list[4].value)+1
    #cell_list[9].value = 'LAST UPDATED END'
    tcell_list[10].value = str(datetime.now())
    sheet.update_cells(tcell_list,'USER_ENTERED')

def stripstring(thestring):
    thestring = thestring.strip()
    thestring = thestring.replace('\n', '')
    return thestring

#
#gsheet = EnterGsheet.authsheet()
#worksheetID_list = gsheet.worksheets()
#worksheet_list = list()
#
#for i in range(0, len(worksheetID_list)):
#    worksheet_list.append(worksheetID_list[i].title)

#gsheet = EnterGsheet.authsheet()
#worksheetID_list=gsheet.worksheets()
#worksheet_list = list()
#sheet = gsheet.worksheet('Fall 2018')
#print(sheet.row_count)
#sheet.insert_row(['hello',1,1],2,'USER_ENTERED')

seasonal_anime_url = 'https://myanimelist.net/anime/season'
page = urllib.request.urlopen(seasonal_anime_url)
soup = BeautifulSoup(page, 'html.parser')

seasons_array_parent = list()
for ultag in soup.find_all('div', {'class': 'horiznav_nav'}):
    seasons_array_parent.append(ultag.find_all('a'))
print(len(seasons_array_parent))
#seasons_array = [stripstring(season.text) for season in seasons_array_parent]


#season_title = soup.find_all(class_="on")
#season_title = stripstring(season_title[0].text)
#for ultag in soup.find_all('div', {'class': 'horiznav_nav'}):
#    season_names = ultag.find_all('a')
#
#season_names_1= season_names[1:2#]
#print(season_names_1[0].get('href'))