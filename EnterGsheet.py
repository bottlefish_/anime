import gspread
from oauth2client.service_account import ServiceAccountCredentials

def authsheet():
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('Anime-a69025caefd1.json', scope)
    client = gspread.authorize(creds)


    # Find a workbook by name and open the first sheet
    # Make sure you use the right name here.
    sheet = client.open("anime test")
    return sheet
